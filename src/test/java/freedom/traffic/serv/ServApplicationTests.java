package freedom.traffic.serv;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import freedom.traffic.serv.repositories.ActivationTypeRepository;
import freedom.traffic.serv.repositories.ClientRepository;
import freedom.traffic.serv.repositories.ProdInfoColRepository;
import freedom.traffic.serv.repositories.ProductInfoRepository;
import freedom.traffic.serv.repositories.ProductRatesRepository;

@SpringBootTest
class ServApplicationTests {

	@Autowired
	private ClientRepository clientRepo;

	@Autowired
	private ActivationTypeRepository actTypeRepo;

	@Autowired
	private ProductInfoRepository prodInfoRepo;

	@Autowired 
    private ProductRatesRepository prodRateRepo;

    @Autowired
    private ProdInfoColRepository prodInfoColRepo;

	@Test
	void contextLoads() {
	}


	@Test
	void initDbTest(){
		System.out.println("ACTIVATION TYPES:");
		var actTypes = actTypeRepo.findAll();
		actTypes.forEach(val -> {
			System.out.println("\t" + val);
		});

		System.out.println("PRODUCT RATE COLLS:");
		var colls = prodInfoColRepo.findAll();
		colls.forEach(val -> {
			System.out.println("\t" + val);
		});

		System.out.println("PRODUCT RATES:");
		var rates = prodRateRepo.findAll();
		rates.forEach(val -> {
			System.out.println("\t" + val);
		});

		System.out.println("PRODUCTS:");
		var products = prodInfoRepo.findAll();

		if (!products.iterator().hasNext())
		products.forEach(val -> {
			System.out.println("\t" + val);
		});

		System.out.println("CLIENTS:");
		var allClients = clientRepo.findAll();
		allClients.forEach(val -> {
			System.out.println("\t" + val);
		});
	}
}
