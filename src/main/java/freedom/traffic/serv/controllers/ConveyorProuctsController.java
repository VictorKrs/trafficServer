package freedom.traffic.serv.controllers;

import java.util.List;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import freedom.traffic.serv.interfaces.CommonFunctions;
import freedom.traffic.serv.models.ErrorResponseData;
import freedom.traffic.serv.models.ProductInfo;
import freedom.traffic.serv.models.enums.ActionTypes;
import freedom.traffic.serv.models.responses.AvailableProductResponse;
import freedom.traffic.serv.models.responses.ProductInfoResponse;
import freedom.traffic.serv.repositories.ProdInfoColRepository;
import freedom.traffic.serv.repositories.ProductInfoRepository;
import freedom.traffic.serv.repositories.ProductRatesRepository;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/conveyor-products")
@Slf4j
public class ConveyorProuctsController {

    @Autowired
    private CommonFunctions commonFunctions;

    @Autowired
    private ProductInfoRepository prodRepo;
    @Autowired
    private ProductRatesRepository prodRateRepo;
    @Autowired
    private ProdInfoColRepository prodInfoColRepo;


    @GetMapping("/open/")
    public ResponseEntity<Object> getProductsOnOpen(@RequestParam("lead_uuid") String uuid,
            @RequestHeader("language") String lang) {
        log.info("GetProductsOpen; UUID: " + uuid + "; lang: " + lang);
        
        var error = new ErrorResponseData();
        var leadInfo = commonFunctions.checkLead(uuid, error);

        if (leadInfo == null){
            return new ResponseEntity<>(error.getMessage(), error.getStatus());
        }
        
        if (!leadInfo.getActionType().equals(ActionTypes.Open)) {
            return new ResponseEntity<>("Error actionType for lead; UUID: " + uuid, HttpStatus.BAD_REQUEST);
        }

        List<ProductInfo> avProducts = null;

        if (leadInfo.getIsResident()) {
            avProducts = prodRepo.findAllByAvailableToResident(true);
        } else {
            avProducts = prodRepo.findAllByAvailableToNoResident(true);
        }

        var result = new ArrayList<AvailableProductResponse>();

        avProducts.forEach(val -> {
            var avProd = new AvailableProductResponse();

            avProd.setName(val.getTitle());
            avProd.setUuid(val.getUuid().toString());
            avProd.setShortDescription(val.getText());
            avProd.setImageList(val.getIconUrl());
            avProd.setType(val.getType());
            avProd.setCanOpenDigitalCard(val.isCanOpenDigitalCard());

            result.add(avProd);
        });

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{uuid}/open/")
    public ResponseEntity<Object> getProductInfo(@PathVariable("uuid") String prodUuid,
            @RequestParam("lead_uuid") String leadUuid,
            @RequestHeader("language") String lang) {
        log.info("GetProductInfo; prodUuid: " + prodUuid + "; lead_uuid: " + leadUuid + "; language: " + lang);

        var error = new ErrorResponseData();
        var leadInfo = commonFunctions.checkLead(leadUuid, error);

        if (leadInfo == null){
            return new ResponseEntity<>(error.getMessage(), error.getStatus());
        }
        
        var prodOpt = prodRepo.findByUuid(UUID.fromString(prodUuid));
        if (!prodOpt.isPresent()){
            log.info("Not found product by UUID: " + prodUuid);

            return new ResponseEntity<>("Not found product by UUID: " + prodUuid, HttpStatus.NOT_FOUND);
        }

        var prodInfo = prodOpt.get();
        var prodInfoResp = new ProductInfoResponse(prodInfo);
        var rates = prodRateRepo.findAllByProdId(prodInfo.getId());
        
        if (rates.size() == 0){
            log.error("RATES NOT FOUND!!!; ProdId: " + prodInfo.getId() + "; Product UUID: " + prodUuid);
        }

        rates.forEach(val -> {
            val.setValues(prodInfoColRepo.findAllByRateId(val.getId()));
        });

        prodInfoResp.setRates(rates);

        return new ResponseEntity<>(prodInfoResp, HttpStatus.OK);
    }
}