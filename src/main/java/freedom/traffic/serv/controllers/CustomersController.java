package freedom.traffic.serv.controllers;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import freedom.traffic.serv.interfaces.CustomersService;
import freedom.traffic.serv.models.Lead;
import freedom.traffic.serv.models.requests.CreateLeadRequestData;
import freedom.traffic.serv.models.responses.CreateLeadResponseData;
import freedom.traffic.serv.repositories.LeadRepository;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/customers")
@Slf4j
public class CustomersController {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private CustomersService procService;

    @PostMapping("/leads/")
    public CreateLeadResponseData createLead(@RequestBody CreateLeadRequestData data) throws Exception{
        log.info("Request data: " + data);

        var lead = new Lead();
        lead.setActionType(data.getAction());
        lead.setIsResident(data.getIsResident());
        lead.setCreatedAt(new Date());

        var inserted = leadRepository.save(lead);

        if (data.getIsResident()) {
            procService.onLeadProcess(lead);
        }

        var response = new CreateLeadResponseData();
        response.setUuid(inserted.getUuid().toString());

        return response;
    }

    @GetMapping("/leads/{uuid}")
    public Optional<Lead> getLead(@PathVariable("uuid") String uuid){
        return leadRepository.findById(UUID.fromString(uuid));
    }
}
