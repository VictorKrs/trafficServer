package freedom.traffic.serv.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import freedom.traffic.serv.interfaces.CustomersService;
import freedom.traffic.serv.interfaces.WebSocketMessageSender;
import freedom.traffic.serv.models.Lead;
import freedom.traffic.serv.models.enums.ActionTypes;
import freedom.traffic.serv.models.enums.WebSocketMessages;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomersServiceImp implements CustomersService {

    @Autowired
    private WebSocketMessageSender wsHandler;
    

    @Async
    @Override
    public void onLeadProcess(Lead data) throws Exception{
        log.info("START onLeadProcess");
        Thread.sleep(1000);
        
        if (data.getActionType().equals(ActionTypes.Open)){
            wsHandler.sendMessage(data.getUuid().toString(), WebSocketMessages.SHOW_OPEN_PRODUCTS.toString(), false);
        }

        log.info("END onLeadProcess");
    }
}
