package freedom.traffic.serv.controllers;

import java.io.IOException;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api/common")
@Slf4j
public class CommonController {

    @GetMapping(
        value = "/images/{im}",
        produces = MediaType.IMAGE_PNG_VALUE
    )
    public @ResponseBody byte[] getImage(@PathVariable("im") String im) throws IOException {
        log.info("getImage; im: " + im);

        var file = ResourceUtils.getFile("classpath:static/images/" + im);
        var in = new FileInputStream(file);

        return IOUtils.toByteArray(in);
    }
}
