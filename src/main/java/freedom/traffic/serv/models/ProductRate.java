package freedom.traffic.serv.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import freedom.traffic.serv.models.enums.VeiwTypes;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="prod_id", nullable = false, insertable=false, updatable=false)
    @JsonIgnore
    private ProductInfo productInfo;

    @Column(name = "prod_id")
    @JsonIgnore
    private Long prodId;

    @JsonProperty("order")
    private int order_val;
    private String title;

    @Enumerated(EnumType.STRING)
    @JsonProperty("view_type")
    private VeiwTypes veiwType;

    @Transient
    private List<ProdInfoCol> values;

    public ProductRate(ProductInfo productInfo, int order, String title, VeiwTypes veiwType) {
        this.id = null;
        this.productInfo = productInfo;
        this.prodId = productInfo.getId();
        this.order_val = order;
        this.title = title;
        this.veiwType = veiwType;
    }
    // @OneToMany(mappedBy = "productRate")
    // private Set<ProdInfoCol> values;
}
