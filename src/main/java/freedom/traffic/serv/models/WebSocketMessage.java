package freedom.traffic.serv.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WebSocketMessage {

    private String status;
    private String isError;
    private String message;
}
