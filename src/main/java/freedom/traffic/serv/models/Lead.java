package freedom.traffic.serv.models;

import java.util.Date;
import java.util.UUID;

import freedom.traffic.serv.models.enums.ActionTypes;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    private Boolean isResident;

    private Date createdAt;

    private UUID appUuid = null;
    
    // @Enumerated(EnumType.STRING)
    // private LeadStatus status;
    
    @Enumerated(EnumType.STRING)
    private ActionTypes actionType;

    @PrePersist
    public void createdAt(){
        this.createdAt = new Date();
    }
}
