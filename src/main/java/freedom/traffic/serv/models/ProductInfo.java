package freedom.traffic.serv.models;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class ProductInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private UUID uuid;

    private String image_list;
    private String iconUrl;
    private String title;
    private String text;
    private String type;
    private boolean canOpenDigitalCard = false;
    private boolean availableToResident;
    private boolean availableToNoResident;

    // @ManyToMany(mappedBy = "availableProducts")
    // private Set<Client> clientProducts;

    // @OneToMany(mappedBy = "productInfo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    // private Set<ProductRate> productRates; 

    @PrePersist
    public void uuidPrePersist(){
        uuid = UUID.randomUUID();
    }
}
