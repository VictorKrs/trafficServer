package freedom.traffic.serv.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProdInfoCol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="rate_id", nullable = false, insertable = false, updatable = false)
    @JsonIgnore
    private ProductRate productRate;
    @Column(name="rate_id")
    @JsonIgnore
    private Long rateId;


    @JsonProperty("order")
    private int order_val;

    private String text;

    public ProdInfoCol(ProductRate productRate, int order, String text) {
        this.id = null;
        this.productRate = productRate;
        this.rateId = productRate.getId();
        this.order_val = order;
        this.text = text;
    }
}
