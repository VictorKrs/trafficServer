package freedom.traffic.serv.models;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ErrorResponseData {

    private String message;
    private HttpStatus status;
}
