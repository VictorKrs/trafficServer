package freedom.traffic.serv.models.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import freedom.traffic.serv.models.enums.ActionTypes;
import lombok.Data;

@Data
public class CreateLeadRequestData {

    private boolean isResident;
    private ActionTypes action;

    @JsonProperty("is_resident")
    public boolean getIsResident() {
        return isResident;
    }

    @JsonProperty("is_resident")
    public void setIsResident(boolean value){
        isResident = value;
    }
}
