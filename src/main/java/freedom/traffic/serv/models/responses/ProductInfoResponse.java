package freedom.traffic.serv.models.responses;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import freedom.traffic.serv.models.ProductInfo;
import freedom.traffic.serv.models.ProductRate;
import lombok.Data;

@Data
public class ProductInfoResponse {

    @JsonProperty("name")
    private String title;
    @JsonProperty("short_description")
    private String text;
    private String uuid;
    @JsonProperty("image_detail")
    private String imageUrl;
    @JsonProperty("reissue_fee")
    private int reissueCost;
    @JsonProperty("open_card_fee")
    private int openCost;
    private String type;
    @JsonProperty("can_open_digital_card")
    private Boolean canOpenDogitalCard;
    private List<ProductRate> rates;

    public ProductInfoResponse(ProductInfo prodInfo) {
        title = prodInfo.getTitle();
        text = prodInfo.getText();
        uuid = prodInfo.getUuid().toString();
        imageUrl = prodInfo.getImage_list();
        reissueCost = 0;
        openCost = 0;
        type = prodInfo.getType();
        canOpenDogitalCard = prodInfo.isCanOpenDigitalCard();
    }
}
