package freedom.traffic.serv.models.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AvailableProductResponse {

    private String uuid;
    private String name;

    @JsonProperty("short_description")
    private String shortDescription;
    @JsonProperty("image_list")
    private String imageList;
    private String type;
    @JsonProperty("can_open_digital_card")
    private Boolean canOpenDigitalCard;
}
