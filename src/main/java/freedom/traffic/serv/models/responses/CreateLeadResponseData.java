package freedom.traffic.serv.models.responses;

import lombok.Data;

@Data
public class CreateLeadResponseData {

    private String uuid;
}
