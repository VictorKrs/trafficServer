package freedom.traffic.serv.models.enums;


import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ActionTypes {
    Open("OPEN_CARD"),
    Release("RELEASE_CARD"),
    Reissue("REISSUE_CARD");

    private final String name;

    private static Map<String, ActionTypes> ALL_DATA = Stream
        .of(ActionTypes.values())
        .collect(Collectors.toMap(s -> s.name, Function.identity()));

    private ActionTypes(String name){
        this.name = name;
    }

    public boolean equalsName(String other){
        return name.equals(other);
    }

    public String toString(){
        return this.name;
    }

    @JsonCreator
    public static ActionTypes fromString(String string){
        return Optional
            .ofNullable(ALL_DATA.get(string))
            .orElseThrow(() -> new IllegalArgumentException(string));
    }
}
