package freedom.traffic.serv.models.enums;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum LeadStatus {

    START("START");

    private final String name;

    private static Map<String, LeadStatus> ALL_VALUES = 
        Stream.of(LeadStatus.values())
                .collect(Collectors.toMap(s -> s.name, Function.identity()));

    private LeadStatus(String name) {
        this.name = name;
    }

    public boolean equalsName(String name){
        return this.name.equals(name);
    }

    public String toString(){
        return name;
    }

    @JsonCreator
    public static LeadStatus fromString(String name){
        return Optional
                    .ofNullable(ALL_VALUES.get(name))
                    .orElseThrow(() -> new IllegalArgumentException());
    }
}
