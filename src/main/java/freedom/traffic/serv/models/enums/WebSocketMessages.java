package freedom.traffic.serv.models.enums;

public enum WebSocketMessages {
    SHOW_OPEN_PRODUCTS("SHOW_OPEN_PRODUCTS")
    ;

    private final String name;
    private WebSocketMessages(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
