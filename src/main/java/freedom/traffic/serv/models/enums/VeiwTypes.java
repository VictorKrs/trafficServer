package freedom.traffic.serv.models.enums;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum VeiwTypes {
    FULL_WIDTH("FULL_WIDTH"),
    HALF_WIDTH("HALF_WIDTH");

    private static Map<String, VeiwTypes> ALL_DATA = Stream
        .of(VeiwTypes.values())
        .collect(Collectors.toMap(s -> s.name, Function.identity()));

    private final String name;
    
    private VeiwTypes(String name){
        this.name = name;
    }

    public boolean equalsName(String other){
        return name.equals(other);
    }

    public String toString(){
        return this.name;
    }

    @JsonCreator
    public static VeiwTypes fromString(String string){
        return Optional
            .ofNullable(ALL_DATA.get(string))
            .orElseThrow(() -> new IllegalArgumentException(string));
    }
}
