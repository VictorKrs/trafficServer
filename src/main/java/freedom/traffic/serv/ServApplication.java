package freedom.traffic.serv;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

import freedom.traffic.serv.components.DBInitializer;

@SpringBootApplication
@EnableAsync
public class ServApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServApplication.class, args);
	}

	@Bean
	public CommandLineRunner dataLoader(DBInitializer initializer){
		return args -> {
			initializer.initDb();
		};
	}
}
