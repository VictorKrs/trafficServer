package freedom.traffic.serv.repositories;

import java.util.List;

// import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import freedom.traffic.serv.models.ProductRate;

public interface ProductRatesRepository extends CrudRepository<ProductRate, Long> {

    // @Query("select pr from ProductRate pr where pr.prod_id=?1 order by pr.order_val")
    List<ProductRate> findAllByProdId(Long id);
}
