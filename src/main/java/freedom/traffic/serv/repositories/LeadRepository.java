package freedom.traffic.serv.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import freedom.traffic.serv.models.Lead;

public interface LeadRepository extends CrudRepository<Lead, UUID> {

}
