package freedom.traffic.serv.repositories;

import org.springframework.data.repository.CrudRepository;

import freedom.traffic.serv.models.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
