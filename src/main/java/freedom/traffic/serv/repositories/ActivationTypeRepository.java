package freedom.traffic.serv.repositories;

import org.springframework.data.repository.CrudRepository;

import freedom.traffic.serv.models.ActivationType;

public interface ActivationTypeRepository extends CrudRepository<ActivationType, Long> {

}
