package freedom.traffic.serv.repositories;

import org.springframework.data.repository.CrudRepository;

import freedom.traffic.serv.models.ProductInfo;
import java.util.List;
import java.util.Optional;
import java.util.UUID;



public interface ProductInfoRepository extends CrudRepository<ProductInfo, Long> {

    List<ProductInfo> findAllByAvailableToNoResident(boolean availableToNoResident);
    
    List<ProductInfo> findAllByAvailableToResident(boolean availableToNoResident);

    Optional<ProductInfo> findByUuid(UUID uuid);
}
