package freedom.traffic.serv.repositories;

import org.springframework.data.repository.CrudRepository;

import freedom.traffic.serv.models.ProdInfoCol;
import java.util.List;


public interface ProdInfoColRepository extends CrudRepository<ProdInfoCol, Long> {
    List<ProdInfoCol> findAllByRateId(Long rateId);
}
