package freedom.traffic.serv.components;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import freedom.traffic.serv.models.ActivationType;
import freedom.traffic.serv.models.Client;
import freedom.traffic.serv.models.ProdInfoCol;
import freedom.traffic.serv.models.ProductInfo;
import freedom.traffic.serv.models.ProductRate;
import freedom.traffic.serv.models.enums.VeiwTypes;
import freedom.traffic.serv.repositories.ActivationTypeRepository;
import freedom.traffic.serv.repositories.ClientRepository;
import freedom.traffic.serv.repositories.ProdInfoColRepository;
import freedom.traffic.serv.repositories.ProductInfoRepository;
import freedom.traffic.serv.repositories.ProductRatesRepository;

@Component
public class DBInitializer {

    // --------- CONSTANTS ---------
    static final String KARTOMAT_ACT_TYPE = "kartomat";
    static final String QR_ACT_TYPE = "qr";
    static final String SUPERAPP_ACT_TYPE = "superapp";

    static final String DEPOSIT = "DEPOSIT";
    static final String INVEST = "INVEST";
    static final String FREEDOM = "FREEDOM";
    static final String ARAL = "ARAL";
    static final String FREEPAY = "FREEPAY";

    // --------- REPOSITORIES ---------
    @Autowired
    private ClientRepository clientRepo;

    @Autowired
    private ProductInfoRepository prodInfoRepo;

    @Autowired
    private ActivationTypeRepository actTypesRepo;

    @Autowired 
    private ProductRatesRepository prodRateRepo;

    @Autowired
    private ProdInfoColRepository prodInfoColRepo;


    // --------- API ---------
    public void initDb(){
        var actTypes = new HashMap<String, ActivationType>();
        var products = new HashMap<String, ProductInfo>();
        
        saveActivationTypes(actTypes);
        saveProducts(products);
        saveClients(actTypes, products);
    }

    // --------- PRIVATE METHODS ---------
    private <T, K> T saveDataToDb(T data, Map<K, T> container, CrudRepository<T, ?> repo, Function<T, K> keyGetter){
        var inserted = repo.save(data);
        
        container.put(keyGetter.apply(inserted), inserted);

        return inserted;
    }

    private void saveActivationTypes(Map<String, ActivationType> actTypes) {
        saveDataToDb(
            new ActivationType(null, KARTOMAT_ACT_TYPE), 
            actTypes, actTypesRepo, val -> val.getName());
        saveDataToDb(
                new ActivationType(null, QR_ACT_TYPE), 
                actTypes, actTypesRepo, val -> val.getName());
        saveDataToDb(
                new ActivationType(null, SUPERAPP_ACT_TYPE), 
                actTypes, actTypesRepo, val -> val.getName());
    }

    private void saveProducts(Map<String, ProductInfo> products) {
        var imUrlbase = "http://127.0.0.1:8080/api/common/images/"; 

        createDeposit(imUrlbase, products);
        createFreepay(imUrlbase, products);
        createInvest(imUrlbase, products);
        createFreedom(imUrlbase, products);
        createAral(imUrlbase, products);
    }

    private void createDeposit(String imUrlbase, Map<String, ProductInfo> products){
        var prodInfo = saveDataToDb(
                            new ProductInfo(null, null, imUrlbase + "DEPOSIT.png", 
                                            imUrlbase + "DEPOSIT_icon.png", "Deposit card", "something about DEPOSIT", 
                                            DEPOSIT, false, true, true),
                                            products, prodInfoRepo, val-> val.getType());
        
        var orderRates = 0;
        var rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Максимальная ГЭСВ", VeiwTypes.FULL_WIDTH));

        var orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "14,3%#KZT"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1%#USD"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1%#RUB"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1%#EUR"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1%#CNY"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1%#TRY"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1%#AED"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Снятие без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1 000 000 ₸#в месяц"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Переводы без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "1 000 000 ₸#в месяц"));
    }

    private void createInvest(String imUrlbase, Map<String, ProductInfo> products){
        var prodInfo = saveDataToDb(
                            new ProductInfo(null, null, imUrlbase + "INVEST.png", imUrlbase + "INVEST_icon.png", "Invest card", "something about INVEST", INVEST, false, true, true),
                                            products, prodInfoRepo, val-> val.getType());
        
        var orderRates = 0;
        var rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Валюта карты", VeiwTypes.FULL_WIDTH));
        var orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "KZT"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "USD"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "RUB"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "EUR"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Вознаграждение от Freedom Global PLC ", VeiwTypes.FULL_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "10,5% в#тенге"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "5,4% в#долларах"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "3,5% в#евро "));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Снятие без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "500 000 ₸#в месяц"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Переводы без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "500 000 ₸#в месяц"));
    }

    private void createFreedom(String imUrlbase, Map<String, ProductInfo> products){
        var prodInfo = saveDataToDb(
                            new ProductInfo(null, null, imUrlbase + "FREEDOM.png", imUrlbase + "FREEDOM_icon.png", "Freedom card", "something about FREEDOM", FREEDOM, false, true, false),
                                            products, prodInfoRepo, val-> val.getType());
        
        var orderRates = 0;
        var rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Валюта карты", VeiwTypes.FULL_WIDTH));
        var orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "KZT"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "USD"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "RUB"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "EUR"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Снятие без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "500 000 ₸#в месяц"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Переводы без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "500 000 ₸#в месяц"));
    }

    private void createAral(String imUrlbase, Map<String, ProductInfo> products){
        var prodInfo = saveDataToDb(
                            new ProductInfo(null, null, imUrlbase + "ARAL.png", imUrlbase + "ARAL_icon.png", "Araldy Saqta", "something about ARAL", ARAL, true, true, false),
                                            products, prodInfoRepo, val-> val.getType());
        
        var orderRates = 0;
        var rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Валюта карты", VeiwTypes.FULL_WIDTH));
        var orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "KZT"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "USD"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "RUB"));
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "EUR"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Снятие без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "500 000 ₸#в месяц"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Переводы без комиссий", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "500 000 ₸#в месяц"));
    }

    private void createFreepay(String imUrlbase, Map<String, ProductInfo> products){
        var prodInfo = saveDataToDb(
                            new ProductInfo(null, null, imUrlbase + "FREEPAY.png", 
                                            imUrlbase + "FREEPAY_icon.png", "FREEPAY card", "something about FREEPAY", 
                                            FREEPAY, false, true, false),
                                            products, prodInfoRepo, val-> val.getType());
        
        var orderRates = 0;
        var rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Валюта карты", VeiwTypes.FULL_WIDTH));
        var orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "KZT"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Комиссия на снятие", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "5%#от суммы"));

        rate = prodRateRepo.save(new ProductRate(prodInfo, orderRates++, "Комиссия на переводы", VeiwTypes.HALF_WIDTH));
        orderCols = 0;
        prodInfoColRepo.save(new ProdInfoCol(rate, orderCols++, "5%#от суммы"));
    }

    private void saveClients(HashMap<String, ActivationType> actTypes, HashMap<String, ProductInfo> products) {
        clientRepo.saveAll(
            List.of(
                new Client(null, "Ivanov Ivan", "iin_1", "phone_1", true, 
                    Set.of(products.get(DEPOSIT), products.get(INVEST)),
                    Set.of(actTypes.get(KARTOMAT_ACT_TYPE), actTypes.get(QR_ACT_TYPE))
                ),
                new Client(null, "Petrov Petr", "iin_2", "phone_2", true, 
                    Set.of(products.get(FREEDOM), products.get(ARAL)),
                    Set.of(actTypes.get(KARTOMAT_ACT_TYPE), actTypes.get(QR_ACT_TYPE), actTypes.get(SUPERAPP_ACT_TYPE))
                ),
                new Client(null, "Sidorov Sidr", "iin_3", "phone_3", true, 
                    Set.of(products.get(DEPOSIT), products.get(INVEST), products.get(FREEDOM), products.get(ARAL)),
                    Set.of(actTypes.get(KARTOMAT_ACT_TYPE), actTypes.get(QR_ACT_TYPE), actTypes.get(SUPERAPP_ACT_TYPE))
                ),
                new Client(null, "Tesla Nikola", "iin_4", "phone_4", false, 
                    Set.of(products.get(DEPOSIT), products.get(INVEST), products.get(FREEDOM), products.get(ARAL)),
                    Set.of(actTypes.get(KARTOMAT_ACT_TYPE), actTypes.get(QR_ACT_TYPE), actTypes.get(SUPERAPP_ACT_TYPE))
                )
            )
        );
    }
}