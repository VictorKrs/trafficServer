package freedom.traffic.serv.components;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import freedom.traffic.serv.interfaces.CommonFunctions;
import freedom.traffic.serv.models.ErrorResponseData;
import freedom.traffic.serv.models.Lead;
import freedom.traffic.serv.repositories.LeadRepository;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CommonFuncitonsImpl implements CommonFunctions {

    @Autowired
    private LeadRepository leadRepo;

    @Override
    public Lead checkLead(String uuid, ErrorResponseData error) {
        log.info("Check Lead; UUID: " + uuid);

        var leadInfoOpt = leadRepo.findById(UUID.fromString(uuid));

        String message = null;
        HttpStatus status = null;

        if (!leadInfoOpt.isPresent()) {
            log.error("Not found lead; UUID: " + uuid);

            message = "Not found lead by UUID: " + uuid;
            status = HttpStatus.NOT_FOUND;
        }

        if (message == null){
            return leadInfoOpt.get();
        }

        error.setMessage(message);
        error.setStatus(status);

        return null;
    }
}
