package freedom.traffic.serv.components;

import java.util.Map;
import java.util.HashMap;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import freedom.traffic.serv.interfaces.WebSocketMessageSender;
import freedom.traffic.serv.models.WebSocketMessage;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class WsHandler extends TextWebSocketHandler implements WebSocketMessageSender{

    private Map<String, WebSocketSession> sessions = new HashMap<String,WebSocketSession>();

    private final String ATTRIBUTE_KEY_NAME = "uuid";

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        var uuid = (String)session.getAttributes().get(ATTRIBUTE_KEY_NAME);
        log.info("add Session for uuid: " + uuid);
        sessions.put(uuid, session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        var uuid = session.getAttributes().get(ATTRIBUTE_KEY_NAME);

        log.info("remove Session: " + uuid);
        sessions.remove(uuid);
    }

    public void sendMessage(String uuid, String status, Boolean isError) throws Exception{
        WebSocketSession session = null;
        var tryCount = 0;
        var maxTryCount = 3;
        var sleepTime = 1000;

        var wsMessage = new WebSocketMessage(status, isError.toString(), "");
        var textMessage = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(wsMessage);

        while (session == null && tryCount < maxTryCount) {
            log.info("Try send message; UUID: " + uuid + "; Message: " + status);
            session = sessions.get(uuid);

            if (session == null){
                tryCount++;
                Thread.sleep(sleepTime);
            } else {
                session.sendMessage(new TextMessage(textMessage));
            }
        }

        if (session == null){
            log.error("Not found WebSocketSession by UUID: " + uuid);
        }
    }
}
