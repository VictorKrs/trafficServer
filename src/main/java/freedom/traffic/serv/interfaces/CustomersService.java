package freedom.traffic.serv.interfaces;

import freedom.traffic.serv.models.Lead;

public interface CustomersService {

    void onLeadProcess(Lead data) throws Exception;
}
