package freedom.traffic.serv.interfaces;

public interface WebSocketMessageSender {
    void sendMessage(String socketId, String message, Boolean isError)  throws Exception;
}
