package freedom.traffic.serv.interfaces;

import freedom.traffic.serv.models.ErrorResponseData;
import freedom.traffic.serv.models.Lead;

public interface CommonFunctions {
    
    Lead checkLead(String uuid, ErrorResponseData error);
}
